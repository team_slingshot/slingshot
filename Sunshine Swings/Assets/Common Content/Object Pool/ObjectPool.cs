﻿using UnityEngine;
using System.Collections.Generic;


public static class ObjectPool {

    static GameObject poolParent;

    static Dictionary<string, List<GameObject>> objPool = new Dictionary<string, List<GameObject>>();

    public static GameObject GetPoolObj(string requestedObj)
    {
        GameObject foundObj = objPool["Default"][0];

        if (objPool.ContainsKey(requestedObj) == true)//if the key is valid
        {          

            if (objPool[requestedObj].Count <= 2)
            {
                CreatePoolObj(objPool[requestedObj][0], 0);
            }

            foundObj = objPool[requestedObj][1];

            objPool[requestedObj].Remove(foundObj);

            foundObj.transform.parent = null;

            foundObj.SetActive(true);

        }
        else
        {
            Debug.Log("Invalid Key");
        }

        

        return foundObj;
    }

    public static void CreatePoolObj(GameObject obj, int warmCount)
    {
        if (objPool.ContainsKey("Default") == false) // perform initial setup
        {
            poolParent = new GameObject("Object Pool");

            GameObject defaultObj = new GameObject("Default");
            defaultObj.transform.parent = poolParent.transform;

            defaultObj.SetActive(false);
            objPool.Add("Default", new List<GameObject>() { defaultObj });
        }



        if (objPool.ContainsKey(obj.name) == true)
        {
            GameObject newObj = GameObject.Instantiate(objPool[obj.name][0]);
            newObj.name = objPool[obj.name][0].name;

            newObj.transform.parent = poolParent.transform;

            newObj.SetActive(false);

            objPool[newObj.name].Add(newObj);

            
        }
        else
        {
            GameObject originalObj = GameObject.Instantiate(obj);
            originalObj.name = obj.name;
            originalObj.SetActive(false);

            originalObj.transform.parent = poolParent.transform;

            objPool.Add(obj.name, new List<GameObject>() { originalObj });
        }


        WarmPoolObj(obj.name, warmCount);
        
        
        
        
    }

    public static void ReturnPoolObj(GameObject _toReturn)
    {
        _toReturn.SetActive(false);

        _toReturn.transform.parent = poolParent.transform;

        objPool[_toReturn.name].Add(_toReturn);

    }

    public static void WarmPoolObj(string objId, int count)
    {

        for(int objsWarmed = 0; objsWarmed < count; objsWarmed++)
        {
            CreatePoolObj(objPool[objId][0], 0);
        }
    }

}
