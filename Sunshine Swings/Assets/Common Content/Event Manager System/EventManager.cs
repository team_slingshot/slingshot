﻿using UnityEngine;
using System.Collections.Generic;

public class EventManager : MonoBehaviour {

    public delegate void EventDelegate<T>(T a) where T : GameEvent;

    static Dictionary<System.Type, System.Delegate> events = new Dictionary<System.Type, System.Delegate>();

    public static void AddListener<T>(EventDelegate<T> del) where T : GameEvent
    {// take an event and a delegate store them in a dictonayr
        if (events.ContainsKey(typeof(T)))
        {
            events[typeof(T)] = System.Delegate.Combine(events[typeof(T)], del);
        }
        else
        {
            events.Add(typeof(T), del);
        }
    }

    public static void RemoveListener<T>(EventDelegate<T> del) where T : GameEvent
    {
        events[typeof(T)] = System.Delegate.Remove(events[typeof(T)], del);
    }

    public static void Raise(GameEvent e)//notify
    {
        if (events.ContainsKey(e.GetType()))
        {
            events[e.GetType()].DynamicInvoke(e);
        }



    }

    
}
