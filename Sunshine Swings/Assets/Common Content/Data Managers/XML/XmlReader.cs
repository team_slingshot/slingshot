﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;


public static class XmlReader {


    public static List<XmlEntry> ReadXml(string filePath)
    {
        XmlTextReader reader = new XmlTextReader(filePath);

        List<XmlEntry> foundEntries = new List<XmlEntry>();
        

        string currEntryName = "Default";

        while (reader.Read())
        {

            List<XmlElementData> foundElements = new List<XmlElementData>();

            switch (reader.NodeType)
            {              

                case XmlNodeType.Element:

                    

                    if(reader.HasAttributes == true)
                    {
                        currEntryName = reader.Name;

                        for (int i = 0; i < reader.AttributeCount; i++)
                        {
                           reader.MoveToAttribute(i);

                            foundElements.Add(new XmlElementData(reader.Name, reader.Value));


                        }

                        foundEntries.Add(new XmlEntry(currEntryName, foundElements));
                    }

                    

                    break;


            }
        }

        return foundEntries;

    }

}



