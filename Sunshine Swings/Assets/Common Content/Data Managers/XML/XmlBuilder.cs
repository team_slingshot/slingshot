﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;


public static class XmlBuilder {

    static XmlWriter writer;


    public static void BuildXml(string filePath, string fileName, List<XmlEntry> entries)
    {
        writer = XmlWriter.Create(filePath + "\\" + fileName + ".xml");

        writer.WriteStartDocument();//document setup
        writer.WriteStartElement(fileName);


        foreach (XmlEntry entry in entries)
        {
            writer.WriteStartElement(entry.entryName);

            foreach(XmlElementData element in entry.GetAllElements())
            {
                writer.WriteAttributeString(element.name, element.value);
            }            

            writer.WriteEndElement();
        }


        //document cleanup
        writer.WriteEndElement();

        writer.WriteEndDocument();
        writer.Close();

    }


}
