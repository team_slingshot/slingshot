﻿using UnityEngine;
using System.Collections;
using System.IO;

public static class CSVWriter {

    public static void WriteToCsv(string filePath, string[] toWrite)
    {

        string csvString = "";

        foreach(string value in toWrite)
        {
            csvString = csvString + "," + value;
        }


        File.WriteAllText(filePath, csvString);




    }
}
