﻿using UnityEngine;
using System.Collections;
using System.IO;

public static class CSVReader  {


    public static string[] ReadCsv(string filePath, System.StringSplitOptions splitOptions)
    {
        char[] splitChars = { ',' };

        string[] csvData = File.ReadAllText(filePath).Split(splitChars, splitOptions); 



        return csvData;
    }
}
