﻿using System.Collections;
using UnityEditor;
using System.Diagnostics;

using System.IO;

public class GitRunner
{
    public static void GetCommon()
    {
        var psi = new ProcessStartInfo("git");
        psi.WorkingDirectory = EditorApplication.applicationPath;
        psi.Arguments = "clone http://bitbucket.org/teamredcommon/common.git Common";
        Process.Start(psi);
    }

    [MenuItem("Common/Update Common")]
     public static void RunSomeGitYAll()
    {
        if (Directory.Exists(EditorApplication.applicationPath + @"/Common") == false)
        {
            GetCommon();
        }
        else
        {
            var psi = new ProcessStartInfo("git");
            psi.WorkingDirectory = EditorApplication.applicationPath + @"/Common";
            psi.Arguments = "pull";
            Process.Start(psi);
        }

        
    }
}
