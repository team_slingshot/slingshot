﻿using UnityEngine;
using System.Collections;

public class ShopItem : MonoBehaviour {

    [SerializeField]
    Shop shop;

    public int cost;
    public string itemName;

    void OnMouseDown()
    {
        shop.PurchaseItem(this);

        Debug.Log("ASdf");
    }
}
