﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Shop : MonoBehaviour {

    [SerializeField]
    Player player;

    [SerializeField]
    List<GameObject> storeTabs;

    int lastTab;

    bool storeActive;

    void Awake()
    {
        ActivateTab(0);
        storeActive = true;
        EventManager.AddListener<EventEndAdvertisment>(AdShown);
        EventManager.AddListener<EventStartAdvertisment>(DisableStore);
    }

    public void PurchaseItem(ShopItem item)
    {
        if(player.currency >= item.cost)
        {
            EventManager.Raise(new EventPurchaseItem(item));
        }

        EventManager.Raise(new EventPlayAudio("Click"));
    }

    public void ActivateTab(int targetTab)
    {
        if(storeActive == true)
        {
            lastTab = targetTab;

            for (int i = 0; i < storeTabs.Count; i++)
            {
                if (i == targetTab)
                {
                    storeTabs[i].SetActive(true);
                }
                else
                {
                    storeTabs[i].SetActive(false);
                }
            }

            EventManager.Raise(new EventPlayAudio("Click"));
        }

        
    }

    void DisableStore(EventStartAdvertisment e)
    {
        storeActive = false;
    }

    void AdShown(EventEndAdvertisment e)//this prevents multiple tabs from activating following an ad being shown
    {
        storeActive = true;
        ActivateTab(lastTab);

        EventManager.Raise(new EventGameStart());
    }
}