﻿using UnityEngine;
using System.Collections.Generic;


public class AudioManager :MonoBehaviour {


    [SerializeField]
    AudioSource audioPlayer;

    [SerializeField]
    List<AudioClip> audioEffectsClips;

    [SerializeField]
    AudioClip musicClip;

    [SerializeField]
    Dictionary<string, AudioClip> audioDict = new Dictionary<string, AudioClip>();

    bool effectsMuted;
    bool musicMuted;
	
	void Awake()
    {
        EventManager.AddListener<EventPlayAudio>(PlayAudio);
        EventManager.AddListener<EventStopAudio>(StopAudio);
        EventManager.AddListener<EventAudioMute>(MuteAudio);


        foreach (AudioClip clip in audioEffectsClips)
        {
            audioDict.Add(clip.name, clip);
        }
    }

    void Update()
    {
        if(!audioPlayer.isPlaying && !musicMuted)
        {
            audioPlayer.PlayOneShot(musicClip, 0.25f);
        }
    }

    void MuteAudio(EventAudioMute e)
    {
        audioPlayer.mute = e.muteState;
    }

    void PlayAudio(EventPlayAudio e)
    {
        if(!effectsMuted)
        {
            audioPlayer.PlayOneShot(audioDict[e.audioName]);
        }
        
    }

    void StopAudio(EventStopAudio e)
    {
        audioPlayer.Stop();
    }



    public void ChangeMuteStateMusic()
    {
        if(musicMuted == true)
        {
            musicMuted = false;
        }
        else
        {
            musicMuted = true;
            audioPlayer.Stop();
        }
    }

    public void ChangeMuteStateEffects()
    {
        if (effectsMuted == true)
        {
            effectsMuted = false;
        }
        else
        {
            effectsMuted = true;
            audioPlayer.Stop();
        }
    }
}
