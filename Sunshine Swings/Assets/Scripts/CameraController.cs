﻿using UnityEngine;
using System.Collections.Generic;


public class CameraController : MonoBehaviour{

    [SerializeField]
    Dictionary<string, List<Vector2>> camTrackStorage = new Dictionary<string, List<Vector2>>();

    [SerializeField]
    float moveSpeed;

    float currSpeed;

    [SerializeField]
    float moveThreshold;

    [SerializeField]
    float lookTime;

    bool showingLevel = false;
    List<Vector2> followPath;

    [SerializeField]
    GameObject moverObj;

    Vector2 lookLocation;

    int currPathPoint;

    float waitTime;

    bool followingBall = false;

    float startTime;
    float journeyLength;

    bool lookingAtLocation = false;

    Vector2 lookTarget;

    LevelManager levelManager;

    GameObject ball;

    [SerializeField]
    Material postMaterial;
    float matIntensity = 0;
    bool matLerpDir = true;

    void Awake()
    {
        EventManager.AddListener<EventShowLevel>(ShowLevel);
        EventManager.AddListener<EventCamFollow>(FollowTarget);
        EventManager.AddListener<EventLevelOver>(StopFollowing);
        EventManager.AddListener<EventPauseGame>(GamePaused);
        EventManager.AddListener<EventStartLevel>(NewLevelStarted);
        EventManager.AddListener<EventLevelOver>(LevelEnded);
        EventManager.AddListener<EventBallDeath>(ShowDeathLocation);

        currSpeed = moveSpeed;


        //postMaterial = new Material(Shader.Find("Hidden/ShaderTest"));
        
    }

    void Update()
    {


        if(matLerpDir == true)
        {
            matIntensity += Time.deltaTime;
        }
        else
        {
            matIntensity -= Time.deltaTime;
        }


        if(matIntensity > 1 && matLerpDir == true)
        {
            matLerpDir = false;
        }
        else if (matIntensity < 0 && matLerpDir == false)
        {
            matLerpDir = true;
        }


        postMaterial.SetFloat("_Intensity", matIntensity);

        GetComponent<Renderer>().material = postMaterial;


        if (followingBall == true)
        {


            float dist = Vector2.Distance(this.transform.position, moverObj.transform.position);

            if (dist > moveThreshold)
            {
                LerpToPosition(moverObj.transform.position);
            }

        }
        else if (showingLevel == true)
        {
            if (waitTime < 0)
            {
                LerpToPosition(lookTarget);

                if (Vector2.Distance(this.transform.position, lookTarget) < 0.2)
                {
                    currPathPoint++;
                    if (currPathPoint >= followPath.Count)
                    {
                        showingLevel = false;
                    }
                    else
                    {
                        lookTarget = followPath[currPathPoint];
                    }
                }
            }
            else
            {
                waitTime -= Time.deltaTime;
            }




        }

        if (lookingAtLocation == true)
        {
            LerpToPosition(lookTarget);

            waitTime -= Time.deltaTime;

            if (waitTime <= 0)
            {
                lookingAtLocation = false;
                EventManager.Raise(new EventCamFollow(moverObj.transform));
            }
        }
    }



    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, postMaterial);
    }

    void ShowLevel(EventShowLevel e)
    {

        

        if(camTrackStorage.ContainsKey(e.levelName) == false)
        {

            List<Vector2> pathPositions = new List<Vector2>();

            foreach(Transform trans in e.levelCamTrack)
            {
                pathPositions.Add(trans.position);
            }

            camTrackStorage.Add(e.levelName, pathPositions);
        }



        followPath = camTrackStorage[e.levelName];

        if (followPath.Count > 1)
        {
            showingLevel = true;
            currPathPoint = 1;
            
            lookTarget = followPath[1];
            transform.position = new Vector3(followPath[0].x, followPath[0].y, -30);
            currSpeed = moveSpeed;
            waitTime = lookTime;
        }
        else
        {
            this.transform.position = new Vector3(moverObj.transform.position.x, moverObj.transform.position.y, -30);
        }
        
    }

    void LevelEnded(EventLevelOver e)
    {
        followingBall = false;
        lookingAtLocation = false;
        showingLevel = false;
    }

    void NewLevelStarted(EventStartLevel e)
    {
        ball = FindObjectOfType<GolfBall>().gameObject;
        this.transform.position = new Vector3(moverObj.transform.position.x, moverObj.transform.position.y, -30f);
    }

    void ShowDeathLocation(EventBallDeath e)
    {
        waitTime = lookTime;
        lookTarget = e.location;

        lookingAtLocation = true;
        
    }

    void GamePaused(EventPauseGame e)
    {
        if(e.pauseState == true)
        {
            currSpeed = 0;
        }
        else
        {
            currSpeed = moveSpeed;
        }
    }

    void FollowTarget(EventCamFollow e)
    {
        followingBall = true;
        showingLevel = false;
        this.transform.position = new Vector3(e.followTarget.position.x, e.followTarget.position.y, -30f);
    }

    void StopFollowing(EventLevelOver e)
    {
        followingBall = false;
        showingLevel = false;
        this.transform.parent = null;
    }

    void LerpToPosition(Vector2 lookTarget)
    {
        transform.position = Vector3.Lerp(this.transform.position, lookTarget, currSpeed * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, transform.position.y, -30);
    }

    



}
