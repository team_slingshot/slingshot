﻿using UnityEngine;
using System.Collections.Generic;


public class GameInitializer : MonoBehaviour{

	void Start()
    {
        EventManager.Raise(new EventGameStart());
    }
}
