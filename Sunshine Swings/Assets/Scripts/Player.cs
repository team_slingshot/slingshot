﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class Player : MonoBehaviour{

    
    string playerID;

    [SerializeField]
    public int currLives;
    public int maxLives;

    public bool fullLives;

    public int currNewLifeMins;
    public float currNewLifeSecs;

    [HideInInspector]
    public int currency;

    public bool canGetRewarded;

    public int currProgress;//the highest level that the player has access to

    //reference to the Purchaser script to enable purchases
    public CompleteProject.Purchaser purchaseObject;

    void Awake()
    {
        purchaseObject = FindObjectOfType<CompleteProject.Purchaser>();

        EventManager.AddListener<EventGainLife>(AddLife);
        EventManager.AddListener<EventLoseLife>(LoseLife);
        EventManager.AddListener<EventPurchaseItem>(PurchaseItem);
        EventManager.AddListener<EventGainCurrency>(GainCurrency);

        EventManager.AddListener<EventSaveGame>(SavePlayerData);
        EventManager.AddListener<EventLoadData>(LoadPlayerData);


        if (File.Exists(Application.persistentDataPath + "//PlayerData.csv") == true)
        {
            Debug.Log("Found Player Data");
            EventManager.Raise(new EventLoadData());
        }
        else
        {
            playerID = "Player";
            currLives = 5;
            long currTime = System.DateTime.Now.ToBinary();
            currency = 0;
            currProgress = 0;

            string saveString = playerID + "," + currLives.ToString() + "," + currTime.ToString() + "," + currency.ToString() + "," + currProgress.ToString() + "," + "Rewarded";

            System.IO.File.WriteAllText(Application.persistentDataPath + "/PlayerData.csv", saveString);

            Debug.Log("Created Player Data");
        }

        


    }

    void Update()
    {
        if (fullLives == false)
        {
            currNewLifeSecs -= Time.deltaTime;

            if (currNewLifeSecs < 0)
            {
                if (currNewLifeMins >= 1)
                {
                    currNewLifeMins--;
                    currNewLifeSecs = 59.99f;
                }
            }

            if (currNewLifeMins <= 0 && currNewLifeSecs < 0)
            {
                ResetNewLifeTime();
                EventManager.Raise(new EventGainLife());
            }
        }

        if(Input.GetKeyDown(KeyCode.Escape))//Return btn on device
        {
            Application.Quit();
        }
    }

    void LoadPlayerData(EventLoadData e)
    {
        string[] loadedData = CSVReader.ReadCsv(Application.persistentDataPath + "/PlayerData.csv", System.StringSplitOptions.RemoveEmptyEntries);

        for (int i = 0; i < loadedData.Length; i++)
        {
            switch(i)
            {
                case 0://player ID
                    playerID = loadedData[i];
                    break;

                case 1://player Lives
                    currLives =  int.Parse(loadedData[i]);
                    break;

                case 2://last close year

                    System.TimeSpan timeElapsed = System.DateTime.Now - System.DateTime.FromBinary(long.Parse(loadedData[i]));

                    int minsElapsed = timeElapsed.Minutes;

                    currLives = currLives + minsElapsed / 10;

                    if(currLives >= 5 || timeElapsed.Hours > 0)
                    {
                        currLives = 5;
                        fullLives = true;
                        currNewLifeMins = 10;
                        currNewLifeSecs = 0;
                    }
                    else
                    {
                        fullLives = false;
                        currNewLifeMins = minsElapsed % 10;
                        currNewLifeSecs = timeElapsed.Seconds;
                    }

                    

                    break;

                case 3:
                    currency = int.Parse(loadedData[i]);
                    break;

                case 4:
                    currProgress = int.Parse(loadedData[i]);
                    break;

                case 5:

                    if(loadedData[i] == "Rewarded")
                    {
                        canGetRewarded = true;
                    }
                    else
                    {
                        canGetRewarded = false;
                    }

                    break;
            }
        }
    }

    void SavePlayerData(EventSaveGame e)
    {

        long currTime = System.DateTime.Now.ToBinary();

        string saveString = playerID + "," + currLives.ToString() + "," + currTime.ToString() + "," + currency.ToString() + "," + currProgress.ToString() + "," + "Rewarded";

        System.IO.File.WriteAllText(Application.persistentDataPath + "/PlayerData.csv", saveString);

    }

    void PurchaseItem(EventPurchaseItem e)
    {
        

        switch (e.item.itemName)
        {
            case "Life":

                if(fullLives == false)
                {
                    EventManager.Raise(new EventGainLife());
                    currency -= e.item.cost;
                }

                
                break;

            case "Coins300":
                //send a purchase request to the purchase script for 300 coins
                Debug.Log(purchaseObject);
                purchaseObject.BuyConsumable(CompleteProject.Purchaser.PurchaseCoin300);
                
                break;

            case "Coins500":
                purchaseObject.BuyConsumable(CompleteProject.Purchaser.PurchaseCoin500);

                break;

            case "Coins700":
                purchaseObject.BuyConsumable(CompleteProject.Purchaser.PurchaseCoin700);

                break;

            case "Coins1100":
                purchaseObject.BuyConsumable(CompleteProject.Purchaser.PurchaseCoin1100);

                break;
        }


        EventManager.Raise(new EventSaveGame());
    }

    void GainCurrency(EventGainCurrency e)
    {
        currency += e.currencyGain;

        EventManager.Raise(new EventSaveGame());
    }

    void ResetNewLifeTime()
    {
        currNewLifeMins = 10;
        currNewLifeSecs = 0;
    }

    void AddLife(EventGainLife e)
    {
        currLives++;

        EventManager.Raise(new EventUpdateUI());

        if (currLives >= maxLives)
        {
            fullLives = true;
            currLives = maxLives;
            currNewLifeMins = 10;
            currNewLifeSecs = 0;
        }

        EventManager.Raise(new EventSaveGame());
    }

    void LoseLife(EventLoseLife e)
    {
        currLives--;

        if(currLives <= 0)
        {
            currLives = 0;
        }
        
        if(fullLives == true)
        {
            fullLives = false;
            currNewLifeMins = 10;
            currNewLifeSecs = 0;
        }

        EventManager.Raise(new EventUpdateUI());
        


        EventManager.Raise(new EventSaveGame());
    }
}
