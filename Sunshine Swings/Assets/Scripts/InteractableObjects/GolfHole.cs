﻿using UnityEngine;
using System.Collections;

public class GolfHole : MonoBehaviour
{

    public float sinkSpeed = 250;
    public GameObject ball;
    public bool inHole = false;

    // Use this for initialization
    void Start()
    {
        ball = GameObject.FindWithTag("golfball");
    }

    // Update is called once per frame
    void Update()
    {

        if (inHole == true)
        {
            HitHole();

        }

        if ((Vector2.Distance(ball.transform.position, this.transform.position) > 1f))
        {
            inHole = false;
            ball.transform.localScale = new Vector3(1, 1, 1);
        }


    }

    void OnTriggerEnter2D(Collider2D col)
    {

        //move ball to centre of hole, and shrink it as it moves.
        inHole = true;

    }

    public void HitHole()
    {
        ball.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 0f));
        //Set direction to the hole
        Vector2 dir = ball.transform.position - this.transform.position;
        //Add force to the ball
        ball.GetComponent<Rigidbody2D>().AddForce(-dir * sinkSpeed * Time.deltaTime);

        //Make the ball small
        ball.transform.localScale -= new Vector3(0.2f, 0.2f, 0.2f) * Time.smoothDeltaTime;


        //If the ball scale is less than 0.2
        if (ball.transform.localScale.x < 0.5f)
        {

            EventManager.Raise(new EventCamFollow(this.transform));
            EventManager.Raise(new EventLevelOver(true));
            Debug.Log("Ball Size" + transform.localScale.x);
        }

    }
}
