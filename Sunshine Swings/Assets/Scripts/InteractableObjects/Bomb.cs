﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Bomb : MonoBehaviour
{

    Animator animator;

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        //play animation of bomb exploding
        animator.SetBool("Exploded", true);
        StartCoroutine("Exploding", 1f);
        EventManager.Raise(new EventPlayAudio("Explosion"));
        EventManager.Raise(new EventResetBall());
        EventManager.Raise(new EventBallDeath(this.transform.position));


    }

    public IEnumerator Exploding(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        this.gameObject.SetActive(false);
    }
}