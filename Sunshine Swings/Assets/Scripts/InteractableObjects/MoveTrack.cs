﻿using UnityEngine;
using System.Collections.Generic;


public class MoveTrack :MonoBehaviour{

    [SerializeField]
    float moveSpeed;

    [SerializeField]
    GameObject movePrefab;

    [SerializeField]
    Transform startPoint;

    [SerializeField]
    Transform endPoint;

    GameObject moveObject;

    Vector2 moveTarget;

    bool moveDir = true;

    float startTime;
    float journeyLength;

    void Awake()
    {
        moveObject = GameObject.Instantiate(movePrefab);
        
        moveObject.transform.parent = this.gameObject.transform;

        moveObject.transform.position = startPoint.position;

        moveTarget = endPoint.position;
    }

    void Start()
    {
        startTime = Time.time;
        journeyLength = Vector3.Distance(moveObject.transform.position, moveTarget);
    }

    void Update()
    {
        float distCovered = (Time.time - startTime) * moveSpeed;
        float fracJourney = distCovered / journeyLength;

        moveObject.transform.position = Vector2.Lerp(moveObject.transform.position, moveTarget, fracJourney);

        if (Vector2.Distance(moveObject.transform.position, moveTarget) < 0.5f)
        {

            if (moveDir == true)
            {
                moveTarget = endPoint.position;
                moveDir = false;
            }
            else
            {
                moveTarget = startPoint.position;
                moveDir = true;
            }

            startTime = Time.time;
            journeyLength = Vector3.Distance(moveObject.transform.position, moveTarget);
        }
    }

}
