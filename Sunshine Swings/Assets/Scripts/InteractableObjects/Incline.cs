﻿using UnityEngine;
using System.Collections;

public class Incline : MonoBehaviour {

    [SerializeField]
    Vector2 inclineVector;

    Vector2[] vs = new Vector2[4]
        {
        Vector2.right,
        Vector2.up,
        Vector2.left,
        Vector2.down
        };

    void Awake()
    {
        float inclineRot = transform.rotation.eulerAngles.z;

        float inclineX = transform.rotation.eulerAngles.z;
        float inclineY = transform.rotation.eulerAngles.z;


        int index = Mathf.RoundToInt(1 + inclineRot / 90);
        index = index % 4;

        inclineX = vs[index].x;
        inclineY = vs[index].y;


        inclineVector = vs[index];
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        EventManager.Raise(new EventEnterIncline(inclineVector));
    }

    void OnTriggerExit2D(Collider2D col)
    {
        EventManager.Raise(new EventExitIncline());
    }
}
