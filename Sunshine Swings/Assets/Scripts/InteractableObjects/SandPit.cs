﻿using UnityEngine;
using System.Collections.Generic;


public class SandPit : MonoBehaviour{


    void OnTriggerEnter2D(Collider2D col)
    {
        EventManager.Raise(new EventEnterSandPit());
    }

    void OnTriggerExit2D(Collider2D col)
    {
        EventManager.Raise(new EventExitSandPit());
    }



}
