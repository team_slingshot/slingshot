﻿using UnityEngine;
using System.Collections.Generic;


public class DeadZone : MonoBehaviour{

    void OnCollisionEnter2D(Collision2D col)
    {
        EventManager.Raise(new EventResetBall());
        EventManager.Raise(new EventBallDeath(this.transform.position));
        EventManager.Raise(new EventPlayAudio("WaterSplash"));
    }
}
