﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GolfBall : MonoBehaviour
{

    [SerializeField]
    bool trailToggle;

    [SerializeField]
    float fireSpeedModifier;

    //[SerializeField]
    float moveSpeedModifier;

    float speedDecayRate = 0.99f;
    float sandDecayRate = 0.95f;

    float currSpeedDecay;

    [SerializeField]
    GameObject ballMover;

    Rigidbody2D rigid;

    [SerializeField]
    Collider2D col;

    MovementStatus moveStatus;

    Vector2 lastHitPos;

    Vector2 lastVelocity;

    SpriteRenderer ballSprite;

    bool paused = false;

    Vector3 affectorIncline;
    [SerializeField]
    bool ballActive;

    void Awake()
    {
        ballSprite = GetComponent<SpriteRenderer>();

        EventManager.AddListener<EventFire>(Fire);
        EventManager.AddListener<EventBallMoving>(BallMoving);
        EventManager.AddListener<EventFiringMode>(EnterFireMode);
        EventManager.AddListener<EventBallStopped>(BallStopped);
        EventManager.AddListener<EventEnterSandPit>(EnterSandPit);
        EventManager.AddListener<EventExitSandPit>(ExitSandPit);
        EventManager.AddListener<EventEnterIncline>(EnterInclineZone);
        EventManager.AddListener<EventExitIncline>(ExitInclineZone);
        EventManager.AddListener<EventResetBall>(BallReset);
        EventManager.AddListener<EventPauseGame>(PauseGame);
        EventManager.AddListener<EventBallBounce>(BounceBall);
        EventManager.AddListener<EventLevelOver>(LevelOver);
        EventManager.AddListener<EventRemoveLevel>(RemoveBall);

        currSpeedDecay = speedDecayRate;

    }

    void Start()
    {
        ballActive = true;

        ballMover = FindObjectOfType<LevelManager>().ballMover;

        ballMover.transform.position = this.transform.position;
        transform.parent = ballMover.transform;

        rigid = ballMover.GetComponent<Rigidbody2D>();
        rigid.velocity = new Vector2();

        if (col == null)
        {
            col = GetComponent<Collider2D>();

        }

        GetComponent<TrailRenderer>().enabled = trailToggle;
    }

    void RemoveBall(EventRemoveLevel e)
    {

        ballActive = false;
        // Destroy(this.gameObject);
        gameObject.SetActive(false);
    }

    void BallMoving(EventBallMoving e)
    {
        moveStatus = MovementStatus.InMotion;
        //shooter.SetActive(false);
        col.enabled = true;
    }

    void LevelOver(EventLevelOver e)
    {

        rigid.velocity = new Vector2();
        ballActive = false;
        this.transform.position = ballMover.transform.position;
        //Destroy(this.gameObject);
        gameObject.SetActive(false);
    }


    void BounceBall(EventBallBounce e)
    {
        EventManager.Raise(new EventPlayAudio("Collision"));
        EventManager.Raise(new EventPlayAudio("Bounce"));
        rigid.velocity = Vector2.Reflect(lastVelocity, e.bouncePos);
    }



    void BallStopped(EventBallStopped e)
    {
        moveStatus = MovementStatus.Stopped;
        col.enabled = false;
        rigid.velocity = new Vector2();

        rigid.gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -15);

        ballMover.transform.position = this.transform.position;

        EventManager.Raise(new EventFiringMode());
    }

    void EnterFireMode(EventFiringMode e)
    {
        //shooter.SetActive(true);
        //shooter.transform.position = this.transform.position;
    }

    void EnterInclineZone(EventEnterIncline e)
    {
        moveStatus = MovementStatus.Incline;
        affectorIncline = e.incline;
        EventManager.Raise(new EventPlayAudio("Rolling"));
    }

    void ExitInclineZone(EventExitIncline e)
    {
        moveStatus = MovementStatus.InMotion;
    }

    void EnterSandPit(EventEnterSandPit e)
    {
        moveStatus = MovementStatus.Sandpit;
        EventManager.Raise(new EventPlayAudio("SandImpact"));
    }

    void ExitSandPit(EventExitSandPit e)
    {
        moveStatus = MovementStatus.InMotion;
    }

    void BallReset(EventResetBall e)
    {

        rigid.gameObject.transform.position = new Vector3(lastHitPos.x, lastHitPos.y, -15);
        EventManager.Raise(new EventBallStopped());
        ballMover.transform.position = this.transform.position;
        StartCoroutine(BallAlpha());
    }

    public IEnumerator BallAlpha()
    {
        ballSprite.color = new Color(1f, 1f, 1f, 0.5f);
        yield return new WaitForSeconds(0.25f);
        ballSprite.color = new Color(1f, 1f, 1f, 1f);
        yield return new WaitForSeconds(0.25f);
        ballSprite.color = new Color(1f, 1f, 1f, 0.5f);
        yield return new WaitForSeconds(0.25f);
        ballSprite.color = new Color(1f, 1f, 1f, 1f);
    }

    void PauseGame(EventPauseGame e)
    {
        paused = e.pauseState;

        if (paused == true)
        {
            rigid.velocity = new Vector3();
        }
        else
        {
            rigid.velocity = lastVelocity;
        }

        ballMover.transform.position = this.transform.position;
    }

    void Update()
    {
        if (paused == false && ballActive == true)
        {
            switch (moveStatus)
            {
                case MovementStatus.InMotion:


                    rigid.velocity = rigid.velocity * speedDecayRate;

                    StopCheck();

                    lastVelocity = rigid.velocity;

                    break;

                case MovementStatus.Incline:
                    rigid.AddForce(affectorIncline, ForceMode2D.Force);

                    lastVelocity = rigid.velocity;
                    break;

                case MovementStatus.Sandpit:

                    rigid.velocity = rigid.velocity * sandDecayRate;

                    StopCheck();

                    lastVelocity = rigid.velocity;
                    break;

                default:
                    break;

            }
        }






    }

    void StopCheck()
    {
        if (rigid.velocity.magnitude < 0.1)
        {
            rigid.velocity = new Vector2();
            EventManager.Raise(new EventBallStopped());
        }
    }

    void Fire(EventFire e)
    {
        if (ballActive == true)
        {
            float speed = e.fireSpeed * fireSpeedModifier;

            rigid.AddForce(e.firetarget * speed * Time.deltaTime, ForceMode2D.Impulse);

            EventManager.Raise(new EventBallMoving());

            EventManager.Raise(new EventCamFollow(transform));

            lastHitPos = this.transform.position;

            EventManager.Raise(new EventPlayAudio("Collision"));

        }



    }

}


enum MovementStatus
{
    InMotion,
    Stopped,
    Sandpit,
    Incline,
}

