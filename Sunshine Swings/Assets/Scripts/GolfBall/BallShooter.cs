﻿using UnityEngine;
using System.Collections.Generic;

public class BallShooter : MonoBehaviour {

    Vector3 originPoint;

    Vector2 camPos;
    float clickPosX;
    float clickPosY;

    Vector3 releasePos;
    float releaseDist;

    GameObject ball;

    [SerializeField]
    GameObject aimer;

    [SerializeField]
    float predictLength;

    [SerializeField]
    GameObject linePointer;


    LineRenderer predictionLine;

    Vector3[] linePoints;

    [SerializeField]
    float maxPullDist;

    void Awake()
    {      
        EventManager.AddListener<EventFiringMode>(PrepareToFire);
        EventManager.AddListener<EventStartLevel>(SetupForNewLevel);
        EventManager.AddListener<EventLevelOver>(LevelOver);

        gameObject.SetActive(false);

        predictionLine = GetComponent<LineRenderer>();
    }

    void LevelOver(EventLevelOver e)
    {
        this.transform.position = new Vector3();
        gameObject.SetActive(false);
    }
    
    void SetupForNewLevel(EventStartLevel e)
    {
        gameObject.SetActive(true);
        ball = FindObjectOfType<GolfBall>().gameObject;

    }

    void PrepareToFire(EventFiringMode e)
    {

        gameObject.SetActive(true);
        this.transform.position = new Vector3(ball.transform.position.x, ball.transform.position.y, 0);

        originPoint = ball.transform.position;

        aimer.transform.position = ball.transform.position;
        linePointer.transform.position = ball.transform.position;

        linePoints = new Vector3[] { new Vector3(), new Vector3() };//clear current predictionline

        predictionLine.SetPositions(linePoints);

    }


    void OnMouseDown()
    {
        //aimer.SetActive(true);

        camPos = Camera.main.WorldToScreenPoint(transform.position);
        clickPosX = Input.mousePosition.x - camPos.x;
        clickPosY = Input.mousePosition.y - camPos.y;

        aimer.transform.position = ball.transform.position;
        linePointer.transform.position = ball.transform.position;
       
    }


    

    void OnMouseUp()
    {



        releaseDist = Vector2.Distance(aimer.transform.position, ball.transform.position);
        releasePos = aimer.transform.position;


        float speed = releaseDist * 75;

        Vector2 shootTarget = originPoint - releasePos;

        shootTarget.Normalize();

        EventManager.Raise(new EventFire(speed, shootTarget));

        gameObject.SetActive(false);
    }


    void OnMouseDrag()
    {
        Vector2 dragPos = new Vector3(Input.mousePosition.x - clickPosX, Input.mousePosition.y - clickPosY);

        Vector2 worldPos = Camera.main.ScreenToWorldPoint(dragPos);
        

        if(Vector2.Distance(ball.transform.position, worldPos) <= maxPullDist)
        {
            aimer.transform.position = new Vector3(worldPos.x, worldPos.y, -15);
            linePointer.transform.position = (ball.transform.position - aimer.transform.position) + transform.position;
        }

        

        DrawPredictionLine();
    }

    void DrawPredictionLine()
    {
        

        linePoints = new Vector3[] { ball.transform.position, linePointer.transform.position };

        predictionLine.SetPositions(linePoints);

    }


}
