﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour{

    public List<string> levels;

    string lastScene;

    public GameObject ballMover;
    public GameObject shooter;

    Dictionary<string, Level> levelData = new Dictionary<string, Level>();

    public int currShots;


    Level currlevel;

    [SerializeField]
    Player player;


    //Scene mangment//
    void Awake()
	{
        EventManager.AddListener<EventDrawLevel>(DrawLevel);
        EventManager.AddListener<EventRestartLevel>(RestartLevel);
        EventManager.AddListener<EventRemoveLevel>(RemoveLevel);

        EventManager.AddListener<EventBallStopped>(CheckStatus);
        EventManager.AddListener<EventFire>(ShotFired);
        EventManager.AddListener<EventStartLevel>(StartLevel);
        EventManager.AddListener<EventLevelOver>(ResolveLevel);
    }

    void DrawLevel(EventDrawLevel e)
    {
        SceneManager.UnloadScene(lastScene);
        SceneManager.LoadScene(e.targetLevel, LoadSceneMode.Additive);

        lastScene = e.targetLevel;

    }

    void RemoveLevel(EventRemoveLevel e)
    {
        SceneManager.UnloadScene(lastScene);
        EventManager.Raise(new EventSaveGame());

        
    }

    void RestartLevel(EventRestartLevel e)
    {
        EventManager.Raise(new EventRemoveLevel());
        SceneManager.LoadScene(lastScene, LoadSceneMode.Additive);

        EventManager.Raise(new EventCamFollow(ballMover.transform));
    }



    public void GetLevelData(Level data)
    {
        if(!levelData.ContainsKey(lastScene))
        {
            levelData.Add(lastScene, data);
            Debug.Log("Stored level data for " + lastScene);
        }

        currlevel = levelData[lastScene];
    }
    //END Scene mangment//








    //Level Operations
    void ResolveLevel(EventLevelOver e)
    {
        int reward = 0;

        if (e.levelResult == true)
        {
            reward += 5 + (currShots * 10);

            EventManager.Raise(new EventGainCurrency(reward));


        }
        else if (e.levelResult == false)
        {
            EventManager.Raise(new EventLoseLife());
        }


    }

    void StartLevel(EventStartLevel e)
    {

        currShots = currlevel.allowedShots;

        EventManager.Raise(new EventUpdateUI());

        if (currlevel.cameraTrackPath.Count > 0)
        {
            EventManager.Raise(new EventShowLevel(currlevel.cameraTrackPath, lastScene));
        }
    }

    void ShotFired(EventFire e)
    {
        currShots--;
        EventManager.Raise(new EventUpdateUI());
    }

    void CheckStatus(EventBallStopped e)
    {
        if (currShots <= 0)
        {
            EventManager.Raise(new EventLevelOver(false));
        }
    }
}
