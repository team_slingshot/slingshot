﻿using UnityEngine;
using System.Collections;

public class SettingAnimation : MonoBehaviour {

    public Animator musicButton; //Sets the animator for the music button.
    public Animator soundEffectButton; //Sets the animator for the sound effect button.
    public Animator creditsButton; //Sets the animator for the credits button.
    public bool isOn; //Check to see if it is triggered or not.
    public bool isCreditsOn; //Check to see if the credits button is triggered or not.
    public GameObject creditsDetails; //Will show credits text.
    public GameObject creditsSection; // Will make the credits appear.
    public float secondCountdown = 1f; //Acts as a timer to show delay in showing object.
   

	// Use this for initialization
	void Start () {
        creditsSection.SetActive(false);
    }

    public void CreditsTrigger()
    {
        if(isCreditsOn)
        {
            
            creditsButton.SetBool("isTrigger", false);
            creditsButton.SetBool("setIdle", false);
            isCreditsOn = false;
            secondCountdown += Time.deltaTime;
            //creditsSection.SetActive(false);
        }
        else
        {
            creditsSection.SetActive(true);
            creditsButton.SetBool("isTrigger", true);
            creditsButton.SetBool("setIdle", true);
            isCreditsOn = true;
           

            
            
        }

    }

    public void ButtonTrigger()
    {
        if (isOn)
        {
            musicButton.SetBool("isTrigger", false);
            soundEffectButton.SetBool("isTrigger", false);
            soundEffectButton.SetBool("setIdle", false);
            musicButton.SetBool("setIdle", false);
            isOn = false;
        }
        else
        {
            musicButton.SetBool("isTrigger", true);
            soundEffectButton.SetBool("isTrigger", true);
            soundEffectButton.SetBool("setIdle", true);
            musicButton.SetBool("setIdle", true);
            isOn = true;
        }
        
    }
	
    
	// Update is called once per frame
	void Update () {

        if(isCreditsOn)
        {
            secondCountdown -= Time.deltaTime;

            if(secondCountdown <= 0)
            {
                secondCountdown = -0.1f;
            }
        }
        else
        {
            secondCountdown += Time.deltaTime;

            if (secondCountdown >= 1)
            {
                secondCountdown = 1;
            }
        }


        if (secondCountdown < 0)
        {
            creditsDetails.SetActive(true);
        }
        else
        {
            creditsDetails.SetActive(false);
        }

        if(secondCountdown >= 1)
        {
            creditsSection.SetActive(false);
        }
    }
}
