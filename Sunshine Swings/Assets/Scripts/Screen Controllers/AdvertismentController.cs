﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Advertisements;

public class AdvertismentController : MonoBehaviour{

    [SerializeField]
    float adChance;

    float timeRemaining;

    bool showingAd = false;

    bool adRequested;

    [SerializeField]
    Player player;

	void Awake()
    {
        EventManager.AddListener<EventAdvertismentTime>(AdvertismentTime);
        EventManager.AddListener<EventStartAdvertisment>(GetAdvertisment);

    }

    public void PlayerRequestAd()
    {
        adRequested = true;
        EventManager.Raise(new EventPlayAudio("Click"));
        EventManager.Raise(new EventStartAdvertisment());
    }

    void AdvertismentTime(EventAdvertismentTime e)
    {
        if (Random.Range(1, 100) <= adChance)
        {
            EventManager.Raise(new EventStartAdvertisment());
        }
    }

    void GetAdvertisment(EventStartAdvertisment e)
    {
        Debug.Log("Getting Ad");

        EventManager.Raise(new EventAudioMute(true));

        if (player.canGetRewarded == true)//whether or not the player gets a rewarded ad
        {
            if (Advertisement.IsReady("rewardedVideo"))
            {
                var options = new ShowOptions { resultCallback = HandleShowResult };
                Advertisement.Show("rewardedVideo", options);
            }
        }
        else
        {
            if (Advertisement.IsReady())
            {
                Advertisement.Show();
            }
        }

        

    }





    void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");

                if (adRequested == true)
                {
                    EventManager.Raise(new EventGainLife());
                }

                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }

        EventManager.Raise(new EventEndAdvertisment());
        EventManager.Raise(new EventAudioMute(false));

        adRequested = false;

        EventManager.Raise(new EventRemoveLevel());
        EventManager.Raise(new EventGameStart());
        
    }

}
