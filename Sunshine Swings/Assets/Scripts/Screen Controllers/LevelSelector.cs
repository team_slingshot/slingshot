﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class LevelSelector : MonoBehaviour {

    [SerializeField]
    Player player;

    [SerializeField]
    LevelManager levelManager;

    int currLevelBase;//the number upon which level selctors will be added onto

    [SerializeField]
    List<Button> buttons;

    int lastLevel;

    void Awake()
    {
        currLevelBase = 0;
        UpdateButtons();

        EventManager.AddListener<EventLevelOver>(LevelOver);
    }

    

    public void LevelBtnPressed(int targetLevel)
    {

        if (player.currLives >= 1 && currLevelBase + targetLevel <= player.currProgress)
        {
            EventManager.Raise(new EventDrawLevel(levelManager.levels[currLevelBase + targetLevel]));
            lastLevel = currLevelBase + targetLevel;
        }

        

        EventManager.Raise(new EventPlayAudio("Click"));
    }


    void UpdateButtons()
    {
        for(int i = 0; i < buttons.Count; i++)
        {
            buttons[i].GetComponentInChildren<Text>().text = (currLevelBase + i + 1).ToString();

            if(currLevelBase + i >= player.currProgress + 1)
            {
                buttons[i].gameObject.SetActive(false);
            }
            else
            {
                buttons[i].gameObject.SetActive(true);
            }
        }
    }

    void LevelOver(EventLevelOver e)
    {

        if(e.levelResult == true)
        {
            if (lastLevel >= player.currProgress)
            {
                player.currProgress++;
                Debug.Log(player.currProgress);
                Debug.Log("New prgress " + player.currProgress);
            }
        }

        UpdateButtons();

    }

    public void ChangePage(bool changeDirection)
    {
        if(changeDirection == true)
        {
            currLevelBase += 5;

            if (currLevelBase >= levelManager.levels.Count || currLevelBase > player.currProgress)
            {
                currLevelBase -= 5;
            }
        }
        else
        {
            currLevelBase -= 5;

            if (currLevelBase < 0)
            {
                currLevelBase = 0;
            }
        }

        UpdateButtons();

        //Debug.Log("Now showing levels " + currLevelBase + " to " + (currLevelBase + 5));
        
    }

}
