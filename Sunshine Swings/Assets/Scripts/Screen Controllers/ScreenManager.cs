﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections.Generic;


public class ScreenManager : MonoBehaviour
{
    [SerializeField]
    GameObject mainMenuPanel;

    [SerializeField]
    GameObject gameLosePanel;

    [SerializeField]
    GameObject outOfLivesPanel;

    [SerializeField]
    GameObject pausePanel;

    [SerializeField]
    GameObject gameWinPanel;

    [SerializeField]
    GameObject gameUI;

    [SerializeField]
    GameObject levelSelectPanel;

    [SerializeField]
    GameObject storePanel;

    [SerializeField]
    Text shotsRemaining_Txt;

    [SerializeField]
    Text score_Txt;

    [SerializeField]
    List<Text> lives_Txt;

    [SerializeField]
    List<Text> timer_Txt;

    [SerializeField]
    Text currency_Txt;

    [SerializeField]
    Player player;

    [SerializeField]
    LevelManager level;

    bool levelActive = false;

    void Awake()
    {

        EventManager.AddListener<EventStartLevel>(ActivateGameUI);
        EventManager.AddListener<EventLevelOver>(ActivateGameOver);
        EventManager.AddListener<EventGameStart>(ActivateMainMenu);
        EventManager.AddListener<EventChooseLevel>(ActivateLevelChooser);
        EventManager.AddListener<EventUpdateUI>(UpdateGameUI);
        EventManager.AddListener<EventGainCurrency>(ScoreGained);
        EventManager.AddListener<EventStartLevel>(NewLevelStarted);
    }

    void Update()
    {
        UpdateTimer();
        EventManager.Raise(new EventUpdateUI());
    }

    void NewLevelStarted(EventStartLevel e)
    {
        levelActive = true;
    }

    void UpdateGameUI(EventUpdateUI e)
    {
        if(levelActive == true)
        {
            shotsRemaining_Txt.text = "Shots Remaining: " + level.currShots;
        }

        foreach(Text text in lives_Txt)
        {
            text.text = player.currLives.ToString();
        }

        currency_Txt.text = "Currency: " + player.currency;
    }

    void ScoreGained(EventGainCurrency e)
    {
        score_Txt.text = "Reward: " + e.currencyGain + " coins";
    }

    void ActivateLevelChooser(EventChooseLevel e)
    {
        EventManager.Raise(new EventUpdateUI());

        levelSelectPanel.SetActive(true);
        mainMenuPanel.SetActive(false);
        gameLosePanel.SetActive(false);
        gameWinPanel.SetActive(false);
        outOfLivesPanel.SetActive(false);
        gameUI.SetActive(false);

        //timer_Txt.gameObject.SetActive(true);

        storePanel.SetActive(false);

        EventManager.Raise(new EventRemoveLevel());
        EventManager.Raise(new EventPlayAudio("Click"));

    }

    void ActivateMainMenu(EventGameStart e)
    {
        mainMenuPanel.SetActive(true);
        gameLosePanel.SetActive(false);
        gameWinPanel.SetActive(false);
        outOfLivesPanel.SetActive(false);
        gameUI.SetActive(false);
        levelSelectPanel.SetActive(false);
        pausePanel.SetActive(false);

        //timer_Txt.gameObject.SetActive(false);

        storePanel.SetActive(false);

        EventManager.Raise(new EventRemoveLevel());
    }

    public void ActivateStore()
    {
        mainMenuPanel.SetActive(false);
        gameLosePanel.SetActive(false);
        gameWinPanel.SetActive(false);
        outOfLivesPanel.SetActive(false);
        gameUI.SetActive(false);
        levelSelectPanel.SetActive(false);
        pausePanel.SetActive(false);

        storePanel.SetActive(true);

        EventManager.Raise(new EventRemoveLevel());
        EventManager.Raise(new EventPlayAudio("Click"));
    }

    void ActivateGameOver(EventLevelOver e)
    {
        Debug.Log("ActivateGameOver");

        levelActive = false;

        mainMenuPanel.SetActive(false);
        gameUI.SetActive(false);
        levelSelectPanel.SetActive(false);

        Debug.Log("setpanelsfalse");
        if (e.levelResult == true)
        {
            gameLosePanel.SetActive(false);
            gameWinPanel.SetActive(true);
            outOfLivesPanel.SetActive(false);
            //EventManager.Raise(new EventAdvertismentTime());
        }
        else if(player.currLives <= 1)
        {
            Debug.Log("out of lives");
            outOfLivesPanel.SetActive(true);
            gameLosePanel.SetActive(false);
            gameWinPanel.SetActive(false);
            //EventManager.Raise(new EventAdvertismentTime());
        }
        else if (player.currLives >= 2)
        {
            Debug.Log("stupid stupid stupid");
            gameLosePanel.SetActive(true);
            gameWinPanel.SetActive(false);
            outOfLivesPanel.SetActive(false);
            EventManager.Raise(new EventAdvertismentTime());
        }
    }

    void ActivateGameUI(EventStartLevel e)
    {
        mainMenuPanel.SetActive(false);
        gameLosePanel.SetActive(false);
        gameWinPanel.SetActive(false);
        outOfLivesPanel.SetActive(false);
        gameUI.SetActive(true);
        levelSelectPanel.SetActive(false);
        pausePanel.SetActive(false);

        //timer_Txt.gameObject.SetActive(false);
    }

    public void StartGameBtnPressed()
    {
        EventManager.Raise(new EventChooseLevel());
        EventManager.Raise(new EventPlayAudio("Click"));

    }

    public void GamePauseBtnPressed()
    {
        EventManager.Raise(new EventPauseGame(true));//pause game
        pausePanel.SetActive(true);
        EventManager.Raise(new EventPlayAudio("Click"));
    }

    public void GameResumeBtnPressed()
    {
        EventManager.Raise(new EventPauseGame(false));//resume game
        pausePanel.SetActive(false);
        EventManager.Raise(new EventPlayAudio("Click"));
    }

    public void NextLevelPressed()
    {
        mainMenuPanel.SetActive(false);
        gameLosePanel.SetActive(false);
        gameWinPanel.SetActive(false);
        outOfLivesPanel.SetActive(false);
        gameUI.SetActive(false);
        levelSelectPanel.SetActive(true);
        pausePanel.SetActive(false);
        EventManager.Raise(new EventPlayAudio("Click"));
        EventManager.Raise(new EventRemoveLevel());

    }

    public void ReplayLevelPressed()
    {
        mainMenuPanel.SetActive(false);
        gameLosePanel.SetActive(false);
        gameWinPanel.SetActive(false);
        outOfLivesPanel.SetActive(false);
        gameUI.SetActive(false);
        levelSelectPanel.SetActive(false);
        pausePanel.SetActive(false);

        levelActive = false;

        if(player.currLives >= 1)
        {
            EventManager.Raise(new EventRestartLevel());
        }
        EventManager.Raise(new EventPlayAudio("Click"));

    }

    public void MainMenuPressed()
    {

        if(levelActive == true)
        {
            EventManager.Raise(new EventLoseLife());
            EventManager.Raise(new EventRemoveLevel());
            levelActive = false;
        }
        


        
        EventManager.Raise(new EventGameStart());
        EventManager.Raise(new EventPlayAudio("Click"));
    }


    void UpdateTimer()
    {
        foreach (Text text in timer_Txt)
        {

            if(player.currLives < 5)
            {
                if (Mathf.FloorToInt(player.currNewLifeSecs) >= 10)
                {
                    text.text = player.currNewLifeMins + ":" + Mathf.FloorToInt(player.currNewLifeSecs);
                }
                else
                {
                    text.text = player.currNewLifeMins + ":0" + Mathf.FloorToInt(player.currNewLifeSecs);
                }
            }
            else
            {
                text.text = "";
            }

            
        }

        

        
    }
}
