﻿using UnityEngine;
using System.Collections.Generic;


public class EventCamFollow : GameEvent{

    public Transform followTarget;
	
	public EventCamFollow(Transform _target)
	{
        followTarget = _target;
    }



}
