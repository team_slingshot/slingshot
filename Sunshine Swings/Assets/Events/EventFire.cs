﻿using UnityEngine;
using System.Collections;

public class EventFire : GameEvent {

    public float fireSpeed;
    public Vector2 firetarget;

    public EventFire(float _speed, Vector2 _target)
    {
        fireSpeed = _speed;
        firetarget = _target;
    }

}
