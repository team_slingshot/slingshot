﻿using UnityEngine;
using System.Collections.Generic;


public class EventShowLevel : GameEvent{

    public List<Transform> levelCamTrack;
    public string levelName;

    public EventShowLevel(List<Transform> cameraTrackPath, string _levelName)
	{
        levelName = _levelName;
        levelCamTrack = cameraTrackPath;
    }



}
