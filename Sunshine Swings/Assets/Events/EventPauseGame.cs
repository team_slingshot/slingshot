﻿using UnityEngine;
using System.Collections.Generic;


public class EventPauseGame : GameEvent{

    public bool pauseState;//true = pause false = unpause
	
	public EventPauseGame(bool _pauseState)
	{
        pauseState = _pauseState;
    }



}
