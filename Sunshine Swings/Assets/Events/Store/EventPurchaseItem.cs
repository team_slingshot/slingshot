﻿using UnityEngine;
using System.Collections;

public class EventPurchaseItem : GameEvent {

    public ShopItem item;

	public EventPurchaseItem(ShopItem _item)
    {
        item = _item;
    }
}
