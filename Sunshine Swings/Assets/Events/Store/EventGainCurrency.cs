﻿using UnityEngine;
using System.Collections;

public class EventGainCurrency : GameEvent {

    public int currencyGain;

	public EventGainCurrency(int _toGain)
    {
        currencyGain = _toGain;
    }
}
